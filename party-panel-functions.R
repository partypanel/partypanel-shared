# ggSave <- function(filename=default_name(plot), plot = last_plot(),
#                    height=defaultFigHeight, width=defaultFigWidth, units="in",
#                    dpi = 300, device=NULL, type=NULL, bg="transparent", ...) {
#
#   extension <-
#     gsub("^.*\\.(.+)$", "\\1", filename);
#
#   if (is.null(device)) {
#     device <- tolower(extension);
#   }
#
#   if (device == 'jpg') {
#     device <- 'jpeg';
#     if (is.null(type)) {
#       type <- "cairo";
#     }
#   }
#
#   if ((is.null(type)) && (device=="png")) {
#     type <- "cairo-png";
#   }
#
#   if (device=="png") {
#     ggsave(filename=filename, plot=plot, device=device,
#            height=height, width=width, units=units,
#            dpi=dpi, type=type, bg = bg, ...);
#   } else if (device=="svg") {
#     ### The 'svg' device doesn't have a 'type' argument
#     ggsave(filename=filename, plot=plot, device=device,
#            height=height, width=width, units=units,
#            dpi=dpi, bg = bg, ...);
#   } else if (device=="pdf") {
#     ### The 'pdf' device doesn't have a 'type' argument
#     ggsave(filename=filename, plot=plot, device=device,
#            height=height, width=width, units=units,
#            dpi=dpi, bg = bg, ...);
#   } else {
#     ggsave(filename=filename, plot=plot, device=device,
#            height=height, width=width, units=units,
#            dpi=dpi, type=type, bg = bg, ...);
#   }
#   return(invisible(plot));
# }

# strToFilename <- function(str, ext=NULL) {
#   str <- trimws(tolower(str));
#   str <- gsub('\\s', '-', str);
#   str <- gsub('[[:punct:]]*$', '', str);
#   str <- gsub('[[:punct:]]', '-', str);
#   if (is.null(ext)) {
#     return(str);
#   } else {
#     return(paste0(str,
#                   ".",
#                   trimws(tolower(ext))));
#   }
# }

reWrap <- function(x, width=35) {
  x <- trimws(x);
  x <- gsub("\\n", " ", x);
  x <-
    strwrap(x, width=width,
            simplify=FALSE);
  x <-
    lapply(x,
           paste0,
           collapse="\n");
  return(x);
}

# knitAndSave <- function(plotToDraw,
#                         figCaption,
#                         filename = NULL,
#                         figWidth=defaultFigWidth,
#                         figHeight=defaultFigHeight) {
#   if (substr(figCaption,
#              start=nchar(figCaption),
#              stop=nchar(figCaption)) != ".") {
#     figCaption <-
#       paste0(figCaption, ".");
#   }
#
#   if (is.null(filename)) {
#     ### Save as both PNG and SVG
#     ggSave(plotToDraw,
#            file=file.path(workingPath,
#                           strToFilename(figCaption, "png")),
#            width=figWidth,
#            height=figHeight);
#     ggSave(plotToDraw,
#            file=file.path(workingPath,
#                           strToFilename(figCaption, "svg")),
#            width=figWidth,
#            height=figHeight);
#   } else {
#     ggSave(plotToDraw,
#            file=filename,
#            width=figWidth,
#            height=figHeight);
#   }
#
#   ufs::knitFig(plotToDraw,
#                figWidth=figWidth,
#                figHeight=figHeight,
#                figCaption=figCaption);
#
# }

theme_pp <- function() {
  return (theme_bw());
}


### For printing single question easy bars
singleQuestionEasyBar <- function(item,
                                  data = dat,
                                  labelDataframe = labelDf,
                                  outputPath = workingPath,
                                  knitFig = TRUE,
                                  figWidth = 12,
                                  figHeight = 3,
                                  headerLevel=5,
                                  niceHeader=NULL) {

  if (item %in% labelDf$varNames.cln) {
    questionText <- labelDataframe[labelDf$varNames.cln==item, 'questionText'];
    question <- labelDataframe[labelDf$varNames.cln==item, 'subQuestions'];
    leftAnchor <- labelDataframe[labelDf$varNames.cln==item, 'leftAnchors'];
    rightAnchor <- labelDataframe[labelDf$varNames.cln==item, 'rightAnchors'];
  } else {
    questionText <- leftAnchor <- rightAnchor <- "";
    question <- item;
  }

  if (knitFig) {
    if (is.null(niceHeader)) {
      niceHeader <- item;
    }
    pandoc.header(niceHeader, level=headerLevel);
    cat("\n\n");
    cat(paste0("These are the results for question (or rather, variable) '<str>", item, "</str>'."));
    cat("\n\n");
  }

  if ((item %in% labelDf$varNames.cln) && (nchar(trimws(questionText)) > 0)) {
    cat("\n\n");
    cat(paste0("The lead-in for this question was \"<str>", questionText, "</str>\"."));
    cat("\n\n");
  }

  res <- ggEasyBar(data,
                   item,
                   xlab=NULL, ylab=NULL,
                   showInLegend="none", fontColor="white",
                   biAxisLabels=list(leftAnchors=leftAnchor,
                                     rightAnchors=rightAnchor)) +
    scale_y_continuous(breaks=seq(0, 100, 10)) +
    theme(legend.position="none",
          text=element_text(size=22),
          plot.background = element_rect(fill = "transparent",
                                         color = NA),
          panel.grid = element_blank(),
          #axis.ticks.x = element_line(size=1),
          #axis.ticks.length=unit(1, 'line'),
          title = element_text(size=20)) +
    ggtitle(question);

  if (!is.null(outputPath)) {
    ggSave(plot=res,
           file.path(outputPath,
                     strToFilename(item, "png")),
           width=figWidth,
           height=figHeight,
           bg = "transparent");
  }
  if (knitFig) {
    ufs::knitFig(res,
                                 figWidth=figWidth,
                                 figHeight=figHeight,
                                 figCaption = item);
  }
  invisible(res);
}

### For printing single question easy bars
multiQuestionEasyBar <- function(items,
                                 data = dat,
                                 labelDataframe = labelDf,
                                 outputPath = workingPath,
                                 knitFig = TRUE,
                                 figWidth = 12,
                                 figHeight = 1.5+1.5*length(items)) {

  questionText <- labelDataframe[labelDf$varNames.cln %in% items, 'questionText'];
  question <- labelDataframe[labelDf$varNames.cln %in% items, 'subQuestions'];
  leftAnchor <- labelDataframe[labelDf$varNames.cln %in% items, 'leftAnchors'];
  rightAnchor <- labelDataframe[labelDf$varNames.cln %in% items, 'rightAnchors'];

  if ((length(unique(leftAnchor)) == 1) && (length(unique(rightAnchor)) == 1)) {
    leftAnchor <- unique(leftAnchor);
    rightAnchor <- unique(rightAnchor);
  }

  if (length(unique(questionText)) == 1) {
    subQuestions <- question;
    question <- unique(questionText);
  }

  cat("\n\n");
  pandoc.p(paste0("These are the results for questions ", vecTxtQ(items), "."));
  cat("\n\n");

  if (length(unique(questionText)) == 1) {
    subQuestions <- question;
    question <- unique(questionText);
    cat("\n\n");
    pandoc.p(paste0("The lead-in for these questions was <str>", question, "</str>."));
    cat("\n\n");
  }

  res <- ggEasyBar(data,
                   item,
                   xlab=NULL, ylab=NULL,
                   showInLegend="none", fontColor="white",
                   biAxisLabels=list(leftAnchors=leftAnchor,
                                     rightAnchors=rightAnchor)) +
    scale_y_continuous(breaks=seq(0, 100, 10)) +
    theme(legend.position="none",
          text=element_text(size=22),
          plot.background = element_rect(fill = "transparent",
                                         color = NA),
          panel.grid = element_blank(),
          #axis.ticks.x = element_line(size=1),
          #axis.ticks.length=unit(1, 'line'),
          title = element_text(size=20)) +
    ggtitle(question);

  if (!is.null(outputPath)) {
    ggSave(plot=res,
           file.path(outputPath,
                     strToFilename(item, "png")),
           width=figWidth,
           height=figHeight,
           bg = "transparent");
  }
  if (knitFig) {
    ufs::knitFig(res,
                                 figWidth=figWidth,
                                 figHeight=figHeight,
                                 figCaption = item);
  }
  invisible(res);
}

ppGetVars <- function(regex) {
  vars <-
    grep(regex,
         names(dat),
         value=TRUE,
         perl=TRUE);
  return(vars);
}

ppGetLabs <- function(vars,
                      includeAnchors=FALSE) {
  labs <-
    reWrap(labelDf[labelDf$varNames.cln %in% vars, 'subQuestions'],
           width=Inf);

  if (includeAnchors) {
    labs <-
      paste0(labs,
             "\n[",
             labelDf[labelDf$varNames.cln %in% vars, 'leftAnchors'],
             " - ",
             labelDf[labelDf$varNames.cln %in% vars, 'rightAnchors'],
             "]");
  }
  return(labs);
}

ppMultiFreqAndDiamonds <- function(regex,
                                   labs=NULL,
                                   includeAnchors=FALSE) {

  vars <-
    ppGetVars(regex);

  if (is.null(labs)) {
    labs <- ppGetLabs(vars=vars,
                      includeAnchors=includeAnchors);
  }

  if (length(vars) == 1) {
    dfrm <-
      as.data.frame(t(as.matrix(table(dat[, vars]))));
    row.names(dfrm) <-
      labs;
  } else {
    dfrm <-
      ufs::multiVarFreq(dat,
                        vars,
                        labels=labs,
                        sortByMean=FALSE);
  }

  print(knitr::kable(dfrm,
                     format='html',
                     table.attr='class="ppKable"'));

  dat <-
    ufs::massConvertToNumeric(dat[, vars, drop=FALSE]);

  print(ufs::meansDiamondPlot(dat,
                              rev(vars),
                              labels=rev(reWrap(labs,
                                                40))));

}

ppMultiResponse <- function(regex,
                            labs=NULL) {

  vars <-
    ppGetVars(regex);

  if (is.null(labs)) {
    labs <- ppGetLabs(vars=vars,
                      includeAnchors=FALSE);
  }

  dfrm <-
    ufs::multiResponse(dat,
                       vars);

  dfrm$Option <- c(labs,
                   "Total");

  print(knitr::kable(dfrm,
                     format='html',
                     table.attr='cellpadding="5" cellspacing="0", border="1"'));

}


